package com.ruslan.security.security.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/secure")
public class SecurityController {


    @GetMapping("/hello")
    public ResponseEntity<HelloDto> helloWorld(){
        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("response", "response header");
        return new ResponseEntity<HelloDto>(new HelloDto("hello world"), responseHeader, HttpStatus.CREATED);
    }


    @GetMapping("/authenticated")
    public HelloDto helloWorld(Principal principal){
        return new HelloDto("hello " + principal.getName());
    }

    @GetMapping("/user")
    public HelloDto helloUser(Principal principal){
        return new HelloDto("accessible only user role " + principal.getName());
    }

    @GetMapping("/admin")
    public HelloDto helloAdmin(Principal principal){
        return new HelloDto("accessible only admin role " + principal.getName());
    }

    @PostMapping("/post")
    public HelloDto helloPostAdmin(@RequestBody  HelloDto helloDto){
        return helloDto;
    }


}
