package com.ruslan.security.security.rest;

import com.ruslan.security.security.config.FilterConfigurerAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;



@RequiredArgsConstructor
@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true, // (1)
        securedEnabled = true, // (2)
        jsr250Enabled = true) // (3)
public class SecurityConf extends WebSecurityConfigurerAdapter {

    private final FilterConfigurerAdapter filterConfigurerAdapter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.apply(filterConfigurerAdapter);
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/secure/hello/**")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/secure/user").hasAnyRole("USER", "ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers("/secure/admin").hasAnyRole("ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/secure/post").authenticated()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/secure/post").permitAll();

        super.configure(http);
    }

    /*@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("ruslan")
                .password("{noop}ruslan")
                .roles("USER")
                .and()
                .withUser("admin")
                .password("{noop}admin")
                .authorities("WRITE_PRIVILEGES", "READ_PRIVILEGES").roles("ADMIN");
    }*/

}
