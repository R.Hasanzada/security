package com.ruslan.security.security.filter;

import com.ruslan.security.security.jwt.JwtService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class JwtAuthFilter extends OncePerRequestFilter {

    private static final String AUTH_HEADER = "Authorization";
    private static final String BEARER = "Bearer";
    private static final String ROLE_CLAIM = "ROLE";

    private final JwtService jwtService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        log.info("Out filter in action: {}", removeBearerHeader(request.getHeader(AUTH_HEADER)));
        Claims claims = jwtService.parseToken(removeBearerHeader(request.getHeader(AUTH_HEADER)));
        Authentication authentication = getAuthenticationBearer(claims);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        log.info("Parsed claims are: {}", claims);
        filterChain.doFilter(request, response);
    }

    private Authentication getAuthenticationBearer(Claims claims){
        List<?> roles = claims.get(ROLE_CLAIM, List.class);
        List<GrantedAuthority> authorityList = roles
                .stream()
                .map(a -> new SimpleGrantedAuthority("ROLE_" + a.toString()))
                .collect(Collectors.toList());
        return new UsernamePasswordAuthenticationToken(claims.getSubject(),"",  authorityList);
    }

    private String removeBearerHeader(String header){
        if(header.startsWith(BEARER)){
            return header.substring(BEARER.length()+1);
        }
        throw new RuntimeException("it is not bearer token");
    }

}
