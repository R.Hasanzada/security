package com.ruslan.security.security;

import com.ruslan.security.security.domain.User;
import com.ruslan.security.security.domain.UserAuthority;
import com.ruslan.security.security.jwt.JwtService;
import com.ruslan.security.security.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.Duration;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
public class SecurityApplication implements CommandLineRunner {

	private final UserRepository userRepository;
	private final JwtService jwtService;

	@Bean
	public BCryptPasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

	public static void main(String[] args) {
		SpringApplication.run(SecurityApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("creating demo user");
		User user = new User();
		user.setUsername("ruslan");
		user.setPassword(passwordEncoder().encode("123456"));
		user.setAccountNonExpired(true);
		user.setAccountNonLocked(true);
		user.setEnabled(true);
		user.setCredentialsNonExpired(true);
		UserAuthority userAuthority = new UserAuthority();
		userAuthority.setAuthority("ROLE_ADMIN");
		user.setAuthorities(Set.of(userAuthority));
		userRepository.save(user);

		String jwt = jwtService.issueToken(user, Duration.ofDays(1));
		log.info("jwt is: {}", jwt );
		log.info("claims from jwt: {}", jwtService.parseToken(jwt));
	}
}
