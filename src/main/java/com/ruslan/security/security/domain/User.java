package com.ruslan.security.security.domain;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
@Data
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private boolean AccountNonExpired;

    private boolean AccountNonLocked;

    private boolean CredentialsNonExpired;

    private boolean Enabled;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<UserAuthority> authorities;


}
